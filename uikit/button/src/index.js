import React from 'react';
import PropTypes from 'prop-types';
import css from './button.scss';


const SIDE_GUTTERS = ['12', '16', '20', '24', '28', '32', '36', '40', '44', '48'];
/**
 * @develop
 */
export default class Button extends React.Component {
    static propTypes = {
        /**
         * Тэг
         */
        tagName: PropTypes.string,
        /**
         * Текст кнопки
         */
        children: PropTypes.any,
        /**
         * CSS класс для root элемента
         */
        className: PropTypes.string,
        /**
         * Цвет
         */
        color: PropTypes.oneOf(['green', 'blue', 'red', 'gold', 'gray']),
        /**
         * Изменяет отображение кнопки на контрастную
         */
        contrast: PropTypes.bool,
        /**
         * Изменяет отображение кнопки на прозрачную
         */
        hollow: PropTypes.bool,
        /**
         * Изменяет отображение кнопки на второстепенную
         */
        secondary: PropTypes.bool,
        /**
         * Изменяет отображение кнопки на информационную
         */
        info: PropTypes.bool,
        /**
         * Изменяет отображение кнопки на неактивную
         */
        disabled: PropTypes.bool,
        /**
         * Элемент внутри кнопки перед текстом. Обертка для него ограниченна 16х16 пикселей
         */
        icon: PropTypes.node,
        /**
         * Элемент внутри кнопки после текста. Обертка для него ограниченна 16х16 пикселей
         */
        iconPosition: PropTypes.oneOf(['before', 'after']),
        /**
         * Показывает анимацию загрузки
         */
        loading: PropTypes.bool,
        /**
         * Отступы слева и справа
         */
        side: PropTypes.oneOf([
            ...SIDE_GUTTERS,
            ...SIDE_GUTTERS.map(gutter => parseInt(gutter, 10))
        ]),
        /**
         * Обработчик клика
         */
        onClick: PropTypes.func
    };
    static defaultProps = {
        tagName: 'button',
        children: '',
        className: '',
        color: 'green',
        contrast: false,
        hollow: false,
        secondary: false,
        info: false,
        disabled: false,
        icon: null,
        iconPosition: 'before',
        side: 12,
        loading: false,
        onClick() {}
    };
    render() {
        const {
            tagName,
            color,
            contrast,
            hollow,
            secondary,
            info,
            icon,
            iconPosition,
            side,
            loading,
            disabled,
            className,
            children,
            ...other
        } = this.props;
        const Tag = `${tagName}`;
        const classes = [css.button, css[`button--sideGutter${side}`]];
        if (info) classes.push(css.buttonInfo);
        else classes.push(css[`button--${color}`]);
        if (contrast) classes.push(css.buttonContrast);
        if (hollow) classes.push(css.buttonHollow);
        if (secondary) classes.push(css.buttonSecondary);
        if (className.length > 0) classes.push(className);
        if (icon && iconPosition === 'before') classes.push(css.buttonHasBefore);
        if (icon && iconPosition === 'after') classes.push(css.buttonHasAfter);
        return (
            <Tag
                className={classes.join(' ')}
                disabled={disabled || loading}
                {...other}
                onClick={this.onClick}
            >
                {icon && iconPosition === 'before' && <span className={css.buttonBefore}>{icon}</span>}
                {children.length > 0 && (
                    <span className={css.buttonText}>{loading ? <Spinner/> : children}</span>
                )}
                {icon && iconPosition === 'after' && <span className={css.buttonAfter}>{icon}</span>}
            </Tag>
        );
    }
}
