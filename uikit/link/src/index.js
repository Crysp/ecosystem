import React from 'react';
import PropTypes from 'prop-types';
import css from './link.scss';


/**
 * @develop
 */
export default class Link extends React.PureComponent {
    static propTypes = {
        /**
         * Текст ссылки
         */
        children: PropTypes.string.isRequired,
        /**
         * URL ссылки
         */
        href: PropTypes.string,
        /**
         * Иконка
         */
        icon: PropTypes.node,
        /**
         * Позиция иконки
         */
        iconPosition: PropTypes.oneOf(['before', 'after']),
        /**
         * Псевдо-ссылка
         */
        pseudo: PropTypes.bool,
        /**
         * Дополнительный CSS класс
         */
        className: PropTypes.string
    };
    static defaultProps = {
        href: '',
        iconPosition: 'before',
        pseudo: false,
        className: ''
    };
    onClick = e => {
        const { href, onClick } = this.props;
        if (href.length === 0) {
            e.preventDefault();
        }
        if (onClick) onClick(e);
    };
    render() {
        const {
            children,
            href,
            icon,
            iconPosition,
            pseudo,
            className,
            ...other
        } = this.props;
        const classes = [css.link];
        if (pseudo) classes.push(css.linkPseudo);
        if (className.length > 0) classes.push(className);
        return (
            <a href={href} className={classes.join(' ')} {...other} onClick={this.onClick}>
                <span className={css.text}>
                    {icon && iconPosition === 'before' && <span className={css.before}>{icon}</span>}
                    <span>{children}</span>
                    {icon && iconPosition === 'after' && <span className={css.after}>{icon}</span>}
                </span>
            </a>
        );
    }
}