import React from 'react';


export default class NotFound extends React.Component {
    render() {
        const { location } = this.props;
        return <div>Страница не найдена <code>{location.pathname}</code></div>;
    }
}