import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import Page from 'Containers/Page';
import './global.scss';


document.addEventListener('DOMContentLoaded', () => {
    ReactDOM.render((
        <BrowserRouter>
            <Page/>
        </BrowserRouter>
    ), document.getElementById('root'));
});