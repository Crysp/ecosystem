#!/usr/bin/env node
'use strict';


// Крашит скрипт на не отлавливаемых ошибках
process.on('unhandledRejection', err => {
    throw err;
});


const fs = require('fs-extra');
const path = require('path');
const chalk = require('chalk');
const commander = require('commander');
const packageJson = require('../package.json');


const program = commander
    .version(packageJson.version)
    .arguments(`<cmd> <name>`)
    .usage(`[promo|app|mail] ${chalk.green('[name]')}`);

program.command(`promo [name]`, 'create static page').alias('p');
program.command(`app [name]`, 'create application').alias('a');
program.command(`mail [name]`, 'create mail').alias('m');

program.parse(process.argv);