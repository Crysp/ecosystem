#!/usr/bin/env node
'use strict';


const fs = require('fs-extra');
const path = require('path');
const spawn = require('cross-spawn');
const chalk = require('chalk');
const commander = require('commander');


const program = commander;

program.parse(process.argv);

const projects = program.args;

if (!projects.length && projects[0] !== 'undefined') {
    console.error('project name required');
    process.exit(1);
}


const sdkRoot = path.resolve(__dirname, '../../..');
let config = {
    promo: {
        root: '.'
    },
    app: {
        root: '.'
    },
    mail: {
        root: '.'
    }
};
if (fs.existsSync(path.resolve(__dirname, '../../../sdk.json'))) {
    const userConfig = require('../../../sdk.json');
    config = Object.assign(config, userConfig);
}
const projectName = projects[0];
const relativePath = `${config.app.root}/${projectName}`;
const projectRoot = path.resolve(sdkRoot, relativePath);


function createApp() {
    const root = projectRoot;

    fs.ensureDirSync(projectRoot);
    console.log(`Creating new application in ${chalk.green(relativePath)}.`);

    fs.copy(path.resolve(__dirname, '../template/app'), root);

    process.chdir(root);

    const command = 'yarn';
    const args = [
        'install',
        '--save',
        '--save-exact',
        '--loglevel',
        'error',
    ];

    spawn(command, args, { stdio: 'inherit' });
}


createApp();