const path = require('path');
const StaticSiteGeneratorPlugin = require('static-site-generator-webpack-plugin');


global.window = {};


module.exports = {
    mode: 'development',
    // target: 'node',
    node: {
        fs: 'empty'
    },
    entry: './src/index.js',
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'dist'),
        /* IMPORTANT!
         * You must compile to UMD or CommonJS
         * so it can be required in a Node context: */
        libraryTarget: 'umd'
    },
    module: {
        rules: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
        }]
    },
    plugins: [
        new StaticSiteGeneratorPlugin({
            locals: {
                // Properties here are merged into `locals`
                // passed to the exported render function
                greet: 'Hello'
            }
        })
    ],
    resolve: {
        modules: ['node_modules']
    }
};