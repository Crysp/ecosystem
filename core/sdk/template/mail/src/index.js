import React from 'react';
import ReactDOMServer from 'react-dom/server';
import mjml2html from 'mjml';
import Mail from './components/Mail';


export default function () {
    const html = ReactDOMServer.renderToStaticMarkup(<Mail/>);
    return mjml2html(html, { minify: process.env.NODE_ENV === 'production' }).html;
};