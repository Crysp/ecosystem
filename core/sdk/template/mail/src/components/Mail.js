import React from 'react';
import Layout from './Layout';


export default class Mail extends React.Component {
    render() {
        return (
            <Layout>

                <mj-section background-url="http://1.bp.blogspot.com/-TPrfhxbYpDY/Uh3Refzk02I/AAAAAAAALw8/5sUJ0UUGYuw/s1600/New+York+in+The+1960's+-+70's+(2).jpg"
                            background-size="cover"
                            background-repeat="no-repeat">

                    <mj-column width="600">

                        <mj-text  align="center"
                                  color="#fff"
                                  font-size="40"
                                  font-family="Open Sans">Slogan here</mj-text>

                        <mj-button background-color="#F63A4D"
                                   href="#">
                            Promotion
                        </mj-button>

                    </mj-column>

                </mj-section>


                <mj-section background-color="#fafafa">
                    <mj-column width="400">

                        <mj-text font-style="italic"
                                 font-size="20"
                                 font-family="Open Sans"
                                 color="#626262">My Awesome Text</mj-text>

                        <mj-text color="#525252">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rutrum enim eget magna efficitur, eu semper augue semper. Aliquam erat volutpat. Cras id dui lectus. Vestibulum sed finibus lectus, sit amet suscipit nibh. Proin nec commodo purus. Sed eget nulla elit. Nulla aliquet mollis faucibus.
                        </mj-text>

                        <mj-button background-color="#F45E43"
                                   href="#">Learn more</mj-button>

                    </mj-column>
                </mj-section>

                <mj-section background-color="white">

                    <mj-column>
                        <mj-image width="200"
                                  src="https://designspell.files.wordpress.com/2012/01/sciolino-paris-bw.jpg" />
                    </mj-column>

                    <mj-column>
                        <mj-text font-style="italic"
                                 font-size="20"
                                 font-family="Open Sans"
                                 color="#626262">
                            Find amazing places
                        </mj-text>

                        <mj-text color="#525252">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rutrum enim eget magna efficitur, eu semper augue semper. Aliquam erat volutpat. Cras id dui lectus. Vestibulum sed finibus lectus.</mj-text>

                    </mj-column>
                </mj-section>

                <mj-section background-color="#fbfbfb">
                    <mj-column>
                        <mj-image width="100" src="http://191n.mj.am/img/191n/3s/x0l.png" />
                    </mj-column>
                    <mj-column>
                        <mj-image width="100" src="http://191n.mj.am/img/191n/3s/x01.png" />
                    </mj-column>
                    <mj-column>
                        <mj-image width="100" src="http://191n.mj.am/img/191n/3s/x0s.png" />
                    </mj-column>
                </mj-section>

                <mj-section background-color="#e7e7e7">
                    <mj-column>
                        <mj-social>
                            <mj-social-element name="facebook" />
                        </mj-social>
                    </mj-column>
                </mj-section>
            </Layout>
        );
    }
}