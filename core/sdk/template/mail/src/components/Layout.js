import React from 'react';
import PropTypes from 'prop-types';
import Header from './Header'


export default class Layout extends React.Component {
    static propTypes = {
        title: PropTypes.string,
        preview: PropTypes.string,
        breakpoint: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    };
    static defaultProps = {
        title: '',
        preview: '',
        breakpoint: 320
    };
    render() {
        const { title, preview, breakpoint, children } = this.props;
        const breakpointInPx = typeof breakpoint === 'number' ? `${breakpoint}px` : breakpoint;
        return (
            <mjml>
                <mj-head>
                    <mj-attributes>
                        <mj-all font-family="Open Sans"/>
                    </mj-attributes>
                    <mj-breakpoint width={breakpointInPx}/>
                    <mj-font name="Open Sans" href="https://fonts.googleapis.com/css?family=Open+Sans"/>
                    <mj-title>{title}</mj-title>
                    <mj-preview>{preview}</mj-preview>
                </mj-head>
                <mj-body background-color="#f0f0f1">
                    <Header/>
                    <mj-spacer height="24px"/>
                    {children}
                </mj-body>
            </mjml>
        );
    }
}