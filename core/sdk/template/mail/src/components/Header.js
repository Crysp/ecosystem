import React from 'react';


export default class Header extends React.Component {
    render() {
        return (
            <mj-section
                background-color="#ffffff"
                full-width="full-width"
                padding="24px 0"
            >
                <mj-column width="180" vertical-align="middle" padding="0">
                    <mj-image
                        width="118"
                        padding="0"
                        align="left"
                        src="https://author24.ru/assets/images/email/logo_rect.png"
                    />
                </mj-column>
                <mj-column width="420" vertical-align="middle" padding="0">
                    <mj-text color="#8d8d8d" align="right" padding="0" line-height="18px">
                        Автор24 — интернет-биржа заказчиков и авторов:<br/>
                        заказ диплома, курсовой, контрольной работы, реферата
                    </mj-text>
                </mj-column>
            </mj-section>
        );
    }
}