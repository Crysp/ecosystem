* `yarn run build` - сборка боевой версии проекта
* `yarn run dev` - сборка dev версии проекта
* `yarn run watch` - сборка dev версии проекта с вотчером
* `yarn run serve` - запуск dev сервера с поддержкой hot-reload
* `yarn run dll` - сборка dll
* `yarn run analyze` - просмотр статистики по бандлу
* `yarn run lint` - статический анализатор кода
* `yarn run browsers` - список поддерживаемых браузеров