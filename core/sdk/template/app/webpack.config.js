const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const env = require('minimist')(process.argv.slice(2));
const isProduction = env.mode === 'production';
const isDevelopment = env.mode === 'development';
const statsOnly = env.env && env.env.stats;


module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: ['babel-loader', 'eslint-loader']
        }, {
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [{
                    loader: 'css-loader',
                    query: {
                        modules: true,
                        localIdentName: isProduction ? '[hash:base64:8]' : '[path][name]__[local]--[hash:base64:5]',
                        camelCase: true,
                        minimize: isProduction,
                        sourceMap: true
                    }
                }, 'sass-loader']
            })
        }, {
            test: /\.svg$/,
            use: [{
                loader: "babel-loader"
            }, {
                loader: "react-svg-loader",
                options: {
                    jsx: true // true outputs JSX tags
                }
            }]
        }]
    },
    plugins: (() => {
        const plugins = [
            new ExtractTextPlugin('styles.css'),
            new webpack.DllReferencePlugin({
                context: __dirname,
                manifest: require('./dist/manifest.json')
            })
        ];
        if (isDevelopment) {
            plugins.push(
                new HtmlWebpackPlugin({
                    template: 'utils/dev-server/index.html'
                })
            );
        }
        if (statsOnly) {
            plugins.push(
                new BundleAnalyzerPlugin()
            );
        }
        return plugins;
    })(),
    resolve: {
        alias: {
            Components: path.resolve(__dirname, 'src/components'),
            Containers: path.resolve(__dirname, 'src/containers')
        }
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        historyApiFallback: true,
        overlay: true,
        stats: {
            children: false,
            modules: false
        }
    },
    stats: {
        children: false,
        modules: false
    }
};