import React from 'react';
import { Switch, Route, NavLink } from 'react-router-dom';
import Logo from 'Components/Logo';
import Home from 'Containers/Home';
import NotFound from 'Containers/NotFound';
import css from './page.scss';


export default class Page extends React.Component {
    render() {
        return (
            <div className={css.layout}>
                <div className={css.welcome}>
                    <Logo className={css.logo}/>
                    <div className={css.title}>Добро пожаловать</div>
                </div>
                <div className={css.menu}>
                    <div className={css.content}>
                        <NavLink
                            to="/"
                            exact
                            className={css.menuLink}
                            activeClassName={css.menuLinkActive}
                        >
                            Главная
                        </NavLink>
                        <NavLink
                            to="/404"
                            className={css.menuLink}
                            activeClassName={css.menuLinkActive}
                        >
                            404
                        </NavLink>
                    </div>
                </div>
                <div className={css.main}>
                    <div className={css.content}>
                        <Switch>
                            <Route exact path="/" component={Home}/>
                            <Route component={NotFound}/>
                        </Switch>
                    </div>
                </div>
                <div className={css.footer}>
                    <div className={css.content}>
                        Автор24 &copy; {(new Date()).getFullYear()}
                    </div>
                </div>
            </div>
        );
    }
}