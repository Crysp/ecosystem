import React from 'react';
import LogoSVG from './logo.svg';


export default props => <LogoSVG {...props}/>;