const path = require('path');
const webpack = require('webpack');


module.exports = {
    entry: {
        dll: [
            'react',
            'react-dom',
            'react-router-dom'
        ]
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].bundle.js',
        library: '[name]_[hash]'
    },
    plugins: [
        new webpack.DllPlugin({
            path: path.resolve(__dirname, 'dist/manifest.json'),
            name: '[name]_[hash]'
        })
    ],
    resolve: {
        extensions: ['.js', '.jsx']
    },
};